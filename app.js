
var AWS = require('aws-sdk');

var asg = new AWS.AutoScaling({region: 'us-east-1'});

/* exports.handler = function(event, context) {
  var asg = new AWS.AutoScaling({region: 'us-east-1'});
  console.log(event);

  asg.describeAutoScalingGroups(function(err, data) {
   if (err) console.log(err, err.stack);
   else     console.log(data);
 }); */

 var params = {
   AutoScalingGroupNames: [
      "tata-sky-asg"
   ]
  };

 asg.describeAutoScalingGroups(params, function(err, data) {
   if (err) console.log(err, err.stack);
   else console.log("Minimum Size: "+data.AutoScalingGroups[0].MinSize);
    console.log("Maximum Size: "+data.AutoScalingGroups[0].MaxSize);
    console.log("Desired Capacity: "+data.AutoScalingGroups[0].DesiredCapacity);

    data.AutoScalingGroups[0].Instances.forEach(function(element) {
        console.log(element);
      });
 });

 var params1 = {
  AutoScalingGroupName: "tata-sky-asg",
  MaxSize: 0,
  MinSize: 0,
  DesiredCapacity: 0
 };

 asg.updateAutoScalingGroup(params1, function(err, data) {
   if (err) console.log(err, err.stack); // an error occurred
   else     console.log(data);           // successful response
 });
